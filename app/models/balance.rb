class Balance < ApplicationRecord
  belongs_to :account
  validates :value,  presence: true

  def value=(value)
    if value.is_a?(String)
      value.gsub!(/[^0-9]/, "").insert(-3, '.').to_f
    end
    self[:value] = value
  end

end
