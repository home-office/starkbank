class BalancesController < ApplicationController
  before_action :set_balance, only: [:show]
  require 'bcrypt'

  # GET /balances/1
  # GET /balances/1.json
  def show;end

  # view for put transfer
  def new_beneficiary;end

  # GET /balances/new
  def new
    @balance = Balance.new
  end

  # GET /balances/1/edit
  def edit
    @balance = Balance.where(id: params[:id]).take
  end

  def create
    @balance = Balance.where(account_id: current_account.id).take
    if params[:balance][:value].to_s.gsub!(/[^0-9]/, '').insert(-3, '.').to_f == 0.0
      flash[:error] = 'Insira um valor maior que 0'
      redirect_to new_balance_path
    else
      if @balance.present?
        new_value = params[:balance][:value].to_s.gsub!(/[^0-9]/, '').insert(-3, '.').to_f
        new_value = @balance.value + new_value
        Balance.where(id: @balance.id).update(value: new_value)
        Extract.create(account_id: current_account.id, description: 'Depósito', donate: current_account.agency, beneficiary: nil, amount: params[:balance][:value], rate: 0)
        flash[:success] = 'Depósito realizado!'
        redirect_to extracts_path
      else
        @balance = Balance.new(balance_params.merge(account_id: current_account.id))
        if @balance.save
          Extract.create(account_id: current_account.id, description: 'Depósito', donate: current_account.agency, beneficiary: nil, amount: params[:balance][:value], rate: 0)
          flash[:success] = 'Depósito realizado!'
          redirect_to extracts_path
        else
          flash[:error] = 'Erro ao ralizar Depósito!'
          redirect_to new_balance_path
        end
      end
    end
  end

  def create_transfer
    beneficiary = Account.where(agency: params[:beneficiary]).take
    account_setings = Account.find(current_account.id)
    value = params[:value].to_s.gsub!(/[^0-9]/, '').insert(-3, '.').to_f
    #validando senha
    if account_setings.valid_password?(params[:password].to_s)
      account_balance = Balance.where(account_id: account_setings.id).take
      # beneficiary exist ?
      if beneficiary.present?
        #account same beneficiary
        if account_setings.agency == params[:beneficiary].to_i
          flash[:error] = 'Favorecido não pode ser depositante!'
          render json: false
        else
          rate = ToolsServices.new.set_rate(value)
          value_and_rate_by_rage = ToolsServices.new.set_rate_by_value(value, rate)
          #afte rate get account balance and verify if exist balance
          if account_balance.value > value_and_rate_by_rage
              ToolsServices.new.set_extract_donate(current_account.id, "Transferência", current_account.agency, params[:beneficiary], value_and_rate_by_rage, rate)
              ToolsServices.new.discout_balance_donate(value_and_rate_by_rage, current_account.id)
              ToolsServices.new.set_extract_donate(beneficiary.id, "Depósito", current_account.agency, params[:beneficiary], value_and_rate_by_rage, rate)
              ToolsServices.new.add_balance_beneficiary(value_and_rate_by_rage, beneficiary.id)
              flash[:success] = 'Transferência realizada!'
              render json: true
          else
            flash[:error] = 'Sem saldo na conta!'
            render json: false

          end
        end
      else
        flash[:error] = 'Conta inválida!'
        render json: false

      end
    else
      flash[:error] = 'Senha inválida!'
      render json: false
    end
  end

  def update
    @balance = Balance.where(account_id: current_account.id).take
    new_value_sake = @balance.value - params[:balance][:value].to_s.gsub!(/[^0-9]/, "").insert(-3, '.').to_f
    if params[:balance][:value].to_s.gsub!(/[^0-9]/, "").insert(-3, '.').to_f == 0.0
      flash[:error] = 'Insira um valor maior que 0'
      redirect_to edit_balance_path(@balance.id)
    else
      if new_value_sake < 0
        flash[:error] = 'Sem saldo na conta!'
        redirect_to edit_balance_path(@balance.id)
      else
        if @balance.update(balance_params.merge(value: new_value_sake))
          Extract.create(account_id: current_account.id, description: "Saque", donate: current_account.agency, beneficiary: nil, amount: params[:balance][:value], rate: 0)
          flash[:success] = 'Saque realizado!'
          redirect_to extracts_path
        else
          flash[:error] = 'Erro ao realizar saque!'
          redirect_to edit_balance_path(@balance.id)
        end
      end
    end

  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_balance
    @balance = Balance.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def balance_params
    params.except(:password)
    params.require(:balance).permit(:value, :account_id, :password)
  end
end
