class ExtractsController < ApplicationController
  include Pagy::Backend
  # GET /extracts
  # GET /extracts.json
  def index
    @q = Extract.where(account_id: current_account.id).order(created_at: :desc).ransack(params[:q])
    @extracts = @q.result()
    @pagy, @extracts = pagy(@extracts)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_extract
    @extract = Extract.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def extract_params
    params.require(:extract).permit(:account_id, :description, :donate, :beneficiary, :ammount, :rate)
  end
end
