class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy]

  # GET /accounts
  # GET /accounts.json
  def show
      @account = current_account
  end


  # GET /accounts/1/edit
  def edit
    @account = current_account
  end

  def edit_password
    @account = current_account
  end

  def update_password
    @account = current_account
    if @account.present?
      if @account.update(account_params)
        bypass_sign_in(@account)
        flash[:success] = 'Atualizada com sucesso!'
        redirect_to root_path
      else
        flash[:warning] = 'Erro ao atualizar senha (Mínimo 6 caracters), vefique os campos!'
        redirect_to accounts_reset_password_path
      end
    else
      flash[:warning] = 'Vefirique os campos'
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    if @account.update(account_params)
      flash[:success] = 'Atualizada com sucesso!'
      redirect_to root_path
    else
      flash[:error] = 'Conta não atualizada!'
      edit_account_path
    end
  end


  def enabled_account
    Account.where(id: current_account).update(status: false)
    flash[:success] = 'Sua conta foi cancelada com sucesso!'
    sign_out(current_account)
    redirect_to root_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_account
    @account = Account.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def account_params
    params.require(:account).permit(:phone, :agency, :cpf, :status, :balance)
  end
end
