class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_account!
  before_action :get_balance_account, :verify_status_account

  def get_balance_account
    if account_signed_in?
      @account_id_balance = Balance.where(account_id: current_account.id).take
    else
    end
  end


  def verify_status_account

    if account_signed_in?
      account_sign_in = Account.where(id: current_account.id, status: false)
      if account_sign_in.present?
        sign_out(current_account)
        flash[:info] = 'Realize seu cadastro!'
        redirect_to root_path
      else
      end
    else
    end
  end
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:cpf, :password, :password_confirmation, :email])
  end

end
