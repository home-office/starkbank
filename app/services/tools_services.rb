class ToolsServices

  #create randou agency tests
  def create_randon_agency
    new_agency = rand(10000..99999)

    return new_agency
  end

  #discount balance
  def discout_balance_donate(value, current_account_id)
    @balance_current_account = Balance.where(account_id: current_account_id).take
    new_value = @balance_current_account.value - value
    Balance.where(id: @balance_current_account.id).update(value: new_value)
  end
  #add balance to beneficy
  def add_balance_beneficiary(value, beneficiary)
    @balance_current_account = Balance.where(account_id: beneficiary).take
    if @balance_current_account.present?
      new_value = @balance_current_account.value + value
      Balance.where(id: @balance_current_account.id).update(value: new_value)
      return true
    else
      Balance.create(account_id: beneficiary, value: value)
      return true
    end
  end

  # put rate to return callbacks
  def set_rate(value)
    time_now = Time.now
    hours_now = time_now.strftime("%H").to_i
    if Date.today.on_weekend?
      puts "estamos no fim de semana 7.0"
      return 7.0
    else
      if hours_now >= 9
        if hours_now <= 18
          puts "estamos na semana 5.0"
          return 5.0
        else
          puts " estamos na semana mas estamos entre 9 e 18 do dia"
          return 7.0
        end
      else
        puts " estamos na semana e estamos fora da hora 9 a 18 do dia"
        return 7.0
      end
    end
  end

# set rate by value discount
  def set_rate_by_value(value, rate)
    if value <= 1000.00
      return value + rate
    else
      return value + 10.00 + rate
    end
  end
# set extract donate
  def set_extract_donate(account_id, description, donate, beneficiary, amount, rate)
   @extract =  Extract.new(account_id: account_id, description: description, donate: donate, beneficiary: beneficiary, amount: amount, rate: rate)
    if @extract.save
      return true
    else
      return false
    end
  end

  # set extract beneficiary
  def set_extract_beneficiary(account_id, description, donate, beneficiary, amount, rate)
    @extract = Extract.new(account_id: account_id, description: description, donate: donate, beneficiary: beneficiary, amount: amount, rate: rate.to_f)
    if @extract.save
      return true
    else
      return false
    end
  end

end