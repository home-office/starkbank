//= require rails-ujs
//= require activestorage
//= require turbolinks

function setupMaskMoney() {
    $('.money').each(function() {
        $(this).maskMoney({prefix:'R$ ', thousands:'.', decimal:',', allowZero: true}).focus();

        if(/\d\.\d$/.test($(this).val())) {
            $(this).val($(this).val().concat("0"));
        }

        if ($(this).val() != "") {
            $(this).maskMoney('mask').focus();
        }
    });
}


document.addEventListener("turbolinks:load", function () {
    $(document).ready(function () {


        $('.collapse').collapse();

        var treeviewMenu = $('.app-menu');

        // Toggle Sidebar
        $("#date").mask("99/99/9999");


        // Activate sidebar treeview toggle
        $("[data-toggle='treeview']").click(function (event) {
            event.preventDefault();
            if (!$(this).parent().hasClass('is-expanded')) {
                treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
            }
            $(this).parent().toggleClass('is-expanded');
        });
        // Set initial active toggle
        $("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');

        //Activate bootstrip tooltips
        $("[data-toggle='tooltip']").tooltip();
        $(document).ready(function () {
            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.phone').mask('0000-0000');
            $('.phone_with_ddd').mask('(00) 00000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {
                reverse: true
            });

            setupMaskMoney();


            $('.collapse').collapse();
            $('.dropdown').on('show.bs.dropdown', function (e) {
                $(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
            });
            $('.dropdown').on('hide.bs.dropdown', function (e) {
                $(this).find('.dropdown-menu').first().stop(true, true).slideUp(300);
            });

        });
    });
});

