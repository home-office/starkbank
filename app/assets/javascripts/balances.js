// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

document.addEventListener("turbolinks:load", function () {
    $(document).ready(function () {
        $("#save").click(function () {
            require_transfer()
        });

    });
});

function validacao() {
    var beneficiary = $("#beneficiary").val();
    var value = $("#value").val();
    var password = $("#password").val();

    if (beneficiary == "") {
        toastr.error("Por favor, insira um beneficiario");
        return false
    }
    if (value == "R$ 0,00") {
        toastr.error("Por favor, insira um valor de transferencia");
        return false
    }
    if (password == "") {
        toastr.error("Por favor, insira sua senha");
        return false
    }
    return true
}

function require_transfer() {
    var isValido = validacao()
    if (isValido) {

        var beneficiary = $("#beneficiary").val();
        var value = $("#value").val();
        var password = $("#password").val();

        $.ajax({
            method: "POST",
            url: '/transfer/create',
            data: {beneficiary: beneficiary, value: value, password: password}
        }).done(function (msg) {
            console.log(msg)
            if (msg){
                toastr.success("Transferencia realizada!");
                window.location.replace("/extracts");
            }else{
                window.location.replace("/transfer");
            }
        });
    }
}


