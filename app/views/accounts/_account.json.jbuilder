json.extract! account, :id, :phone, :agency, :cpf, :status, :balance, :created_at, :updated_at
json.url account_url(account, format: :json)
