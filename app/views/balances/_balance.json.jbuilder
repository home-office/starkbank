json.extract! balance, :id, :value, :account_id, :created_at, :updated_at
json.url balance_url(balance, format: :json)
