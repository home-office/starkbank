class CreateExtracts < ActiveRecord::Migration[5.2]
  def change
    create_table :extracts do |t|
      t.references :account, foreign_key: true
      t.string :description
      t.integer :donate
      t.integer :beneficiary
      t.decimal :amount
      t.decimal :rate

      t.timestamps
    end
  end
end
