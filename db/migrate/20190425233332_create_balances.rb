class CreateBalances < ActiveRecord::Migration[5.2]
  def change
    create_table :balances do |t|
      t.decimal :value, precision: 9, scale: 2
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
