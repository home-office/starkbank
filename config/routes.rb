Rails.application.routes.draw do
  resources :balances
  devise_for :accounts
  resources :extracts
  resources :accounts
  root to: "extracts#index"
  patch '/sake/balance', to: "balances#update", as: "sake_balance"
  post '/balances', to: "balances#create", as: "balances_create"
  post '/transfer/create', to: "balances#create_transfer"
  get 'transfer', to: "balances#new_beneficiary"
  patch "/accounts/:id/edit", to: "accounts#update_password", as: "edit_update_pass"
  get '/reset_password', to: "accounts#edit_password"
  post 'enable_acount', to: "accounts#enabled_account", as: "enabled_account"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
