require 'test_helper'

class ExtractsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @extract = extracts(:one)
  end

  test "should create account" do
    assert_difference('Account.count') do
      post new_account_registration_path, params: { account: { agency: 123456, cpf: "111.111.111-11",email: "teste@teste.com", status: true} }
    end

    assert_redirected_to account_url(Account.last)
  end

  test "should create account" do
    assert_difference('Account.count') do
      post new_account_registration_path, params: { account: { agency: 123464, cpf: "111.111.111-11",email: "teste@teste.com", status: true} }
    end

    assert_redirected_to account_url(Account.last)
  end

  test "should get index" do
    get extracts_path
    assert_response :success
  end


end
