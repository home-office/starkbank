require 'test_helper'

class BalancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @balance = balances(:one)
  end

  test "should create account" do
    assert_difference('Account.count') do
      post new_account_registration_path, params: { account: { agency: 123456, cpf: "111.111.111-11",email: "teste@teste.com", status: true} }
    end
    assert_redirected_to account_url(Account.last)
  end

  test "should create account" do
    assert_difference('Account.count') do
      post new_account_registration_path, params: { account: { agency: 123464, cpf: "111.111.111-11",email: "teste@teste.com", status: true} }
    end

    assert_redirected_to account_url(Account.last)
  end

  test "should get new" do
    get new_balance_path
    assert_response :success
  end

  test "should create balance" do
    assert_difference('Balance.count') do
      post balances_path, params: {balance: {account_id: 1, value: 2000.00}}
    end
    assert_redirected_to balance_url(Balance.last)
  end

  test "should show balance" do
    get balance_path(1)
    assert_response :success
  end


  test "should update balance" do
    patch balance_path(1), params: {balance: {value: 2000.00}}
    assert_redirected_to balance_path(@balance)
  end


  test "Ajax method form transfer teste" do
    post transfer_create_path, params: {account_id: 1, description: "Transferência", donate: 1, beneficiary: 2, amount: 1000.00, rate: 5.0}
  end
end
