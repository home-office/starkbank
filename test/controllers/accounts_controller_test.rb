require 'test_helper'

class AccountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @account = accounts(:one)
  end


  test "should get new" do
    get new_account_registration_path
    assert_response :success
  end

  test "should create account" do
    assert_difference('Account.count') do
      post new_account_registration_path, params: { account: { agency: 123456, cpf: "111.111.111-11",email: "teste@teste.com", status: true} }
    end

    assert_redirected_to account_url(Account.last)
  end

  test "should show account" do
    get account_path(@account)
    assert_response :success
  end

  test "should get edit" do
    get edit_account_path(@account)
    assert_response :success
  end



  test "should update account" do
    patch edit_account_path(@account), params: { account: {   cpf: "000.000.111-56", email: "flaugabriel@gmail.com" } }
    assert_redirected_to edit_account_path(@account)
  end

  test "should enable account" do
    enabled_account_path('Account.count', -1) do
      post account_url(@account)
    end

    assert_redirected_to accounts_url
  end
end
