require 'rails_helper'

RSpec.describe Extract, type: :model do
  context 'Validação de extrato' do
    it 'Se depósito funciona' do
      rate = 0.0
      value = ('50,000.00'.delete(",")).to_f
      if value > 0
        account = Account.new
        balance = Balance.new
        balance.account_id = account.id
        balance.value = 30.00
        operation = Extract.new
        operation.account_id = account.id
        operation.description = "Deposit"
        operation.donate = account.agency
        operation.beneficiary = nil
        operation.created_at = Date.today
        operation.amount = value
        operation.rate = rate
        expect(operation).to_not be_valid
      end
    end

    it 'Se saque funciona' do
      rate = 0.0
      new_value = ('25,000.00'.delete(",")).to_f
      if new_value > 0
        account = Account.new
        balance = Balance.new
        balance.account_id = account.id
        balance.value = 30000.00
        operation = Extract.new
        operation.account_id = account.id
        operation.description = "Sake"
        operation.donate = account.agency
        operation.beneficiary = nil
        operation.created_at = Date.today
        operation.amount = balance.value - new_value
        operation.rate = rate
        expect(operation).to_not be_valid
        expect(balance).to_not be_valid
        expect(account).to_not be_valid
      end
    end

    it 'Se transferencia funciona' do
      new_value_donate = ('5,000.00'.delete(",")).to_f
      if new_value_donate > 0
        donate = Account.new
        beneficiary = Account.new
        balance = Balance.new
        balance.account_id = donate.id
        balance.value = 30000.00
        operation = Extract.new
        operation.account_id = donate.id
        operation.description = "Sake"
        operation.donate = donate.agency
        operation.beneficiary = beneficiary.agency
        operation.created_at = Date.today
        operation.amount = balance.value - new_value_donate
        operation.rate = 5.0
        expect(operation).to_not be_valid
      end
    end
  end
end