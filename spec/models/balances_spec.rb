require 'rails_helper'

RSpec.describe Balance, type: :model do
  it "Se deposita é valido" do
    expect(Balance.create(account_id: 2, value: 2000.00)).to_not be_valid
  end

  it "Se saque é valido" do
      account = Account.new
      balance = Balance.create(account_id: account.id, value: 30000.00)
      balance.value = balance.value - 5000.00
      expect(balance).to_not be_valid
  end


end