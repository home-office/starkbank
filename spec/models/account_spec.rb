require 'rails_helper'

RSpec.describe Account, type: :model do
  it "Se esta conta é valida" do
    expect(Account.create(agency: 123456, email: "teste@teste.com", password: "123456", cpf: "111.111.111-11")).to_not be_valid
  end

  it "Se atualizar a conta é valido" do
    expect(Account.where(id: 1).update(agency: 123456, email: "update@update.com", cpf: "222.222.222-22")).to_not be_valid
  end

  it "Se atualizar a senha é valido" do
    expect(Account.where(id: 1).update(password: "654321")).to_not be_valid
  end

end

